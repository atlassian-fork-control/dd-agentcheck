#! /usr/bin/env python3.5

from time import time
from flask import Flask, jsonify

# https://github.com/DataDog/dd-agent/blob/master/checks/__init__.py#L293

MINIMAL_DATA = {
    'count': {
        'my.counter': {
        },
        'my.other.counter': {
        }
    },
    'decrement': {
        'my.decremented.counter': {
        },
        'my.other.decremented.counter': {
        },
    },
    'gauge': {
        'my.gauge': {
            'value': 5000,
        },
        'my.other.gauge': {
            'value': 3349934,
        }
    },
    'histogram': {
        'my.histogram.metric': {
            'value': 12,
        },
        'my.other.histogram.metric': {
            'value': 3413,
        },
    },
    'increment': {
        'my.incremented.counter': {
        },
        'my.other.incremented.counter': {
        },
    },
    'monotonic_count': {
        'my.monotonic.counter': {
        },
        'my.other.monotonic.counter': {
        },
    },
    'rate': {
        'my.rate.metric': {
            'value': 334,
        },
        'my.other.rate.metric': {
            'value': 34395,
        },
    },
}

FULL_DATA = {
    'count': {
        'my.counter': {
            'value': 343,
            'tags': [
                'mytag:mytagvalue',
                'myothertag:myothertagvalue',
            ],
            'hostname': 'myhostname',
            'device_name': 'mydevicename',
        },
        'my.other.counter': {
            'value': 393434,
            'tags': [
                'mytag:mytagvalue',
                'myothertag:myothertagvalue',
            ],
            'hostname': 'myhostname',
            'device_name': 'mydevicename',
        }
    },
    'decrement': {
        'my.decremented.counter': {
            'value': 33944,
            'tags': [
                'mytag:mytagvalue',
                'myothertag:myothertagvalue',
            ],
            'hostname': 'myhostname',
            'device_name': 'mydevicename',
        },
        'my.other.decremented.counter': {
            'value': 32,
            'tags': [
                'mytag:mytagvalue',
                'myothertag:myothertagvalue',
            ],
            'hostname': 'myhostname',
            'device_name': 'mydevicename',
        },
    },
    'gauge': {
        'my.gauge': {
            'value': 4000,
            'tags': [
                'mytag:mytagvalue',
                'myothertag:myothertagvalue'
            ],
            'hostname': 'myhostname',
            'device_name': 'mydevicename',
        },
        'my.other.gauge': {
            'value': 3349934,
            'tags': [
                'mytag:mytagvalue',
                'myothertag:myothertagvalue'
            ],
            'hostname': 'myhostname',
            'device_name': 'mydevicename',
        }
    },
    'histogram': {
        'my.histogram.metric': {
            'value': 12,
            'tags': [
                'mytag:mytagvalue',
                'myothertag:myothertagvalue',
            ],
            'hostname': 'myhostname',
            'device_name': 'mydevicename',
        },
        'my.other.histogram.metric': {
            'value': 3413,
            'tags': [
                'mytag:mytagvalue',
                'myothertag:myothertagvalue',
            ],
            'hostname': 'myhostname',
            'device_name': 'mydevicename',
        },
    },
    'increment': {
        'my.incremented.counter': {
            'value': 12,
            'tags': [
                'mytag:mytagvalue',
                'myothertag:myothertagvalue',
            ],
            'hostname': 'myhostname',
            'device_name': 'mydevicename',
        },
        'my.other.incremented.counter': {
            'value': 3413,
            'tags': [
                'mytag:mytagvalue',
                'myothertag:myothertagvalue',
            ],
            'hostname': 'myhostname',
            'device_name': 'mydevicename',
        },
    },
    'monotonic_count': {
        'my.monotonic.counter': {
            'value': 945945,
            'tags': [
                'mytag:mytagvalue',
                'myothertag:myothertagvalue',
            ],
            'hostname': 'myhostname',
            'device_name': 'mydevicename',
        },
        'my.other.monotonic.counter': {
            'value': 43,
            'tags': [
                'mytag:mytagvalue',
                'myothertag:myothertagvalue',
            ],
            'hostname': 'myhostname',
            'device_name': 'mydevicename',
        },
    },
    'rate': {
        'my.rate.metric': {
            'value': 334,
            'tags': [
                'mytag:mytagvalue',
                'myothertag:myothertagvalue',
            ],
            'hostname': 'myhostname',
            'device_name': 'mydevicename',
        },
        'my.other.rate.metric': {
            'value': 34395,
            'tags': [
                'mytag:mytagvalue',
                'myothertag:myothertagvalue',
            ],
            'hostname': 'myhostname',
            'device_name': 'mydevicename',
        },
    },
}


app = Flask(__name__)

@app.route('/')
def minimal():
    return jsonify(MINIMAL_DATA)

@app.route('/full')
def full():
    global FULL_DATA
    FULL_DATA['gauge']['my.gauge']['timestamp'] = int(time())
    FULL_DATA['gauge']['my.other.gauge']['timestamp'] = int(time())
    return jsonify(FULL_DATA)

if __name__ == "__main__":
    app.run('0.0.0.0')
