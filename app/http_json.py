"""A general purpose Datadog agent check for web apps.

Example:

    Add a metrics enpoint to you app that exposes JSON that looks like this...

        {
            "gauge": {
                "my.gauge": {
                    "value": 3834,
                    "tags": [
                        "mytag:mytagvalue",
                        "myothertag:myothertagvalue"
                    ]
                }
            }
        }

"""

from hashlib import md5
from random import shuffle
from time import time
from urlparse import urlparse

from checks import AgentCheck
from requests import Timeout, codes, get, put

TARGETS_URL = 'http://localhost:8080/targets'

METHODS = ['count',
           'decrement',
           'gauge',
           'histogram',
           'increment',
           'monotonic_count',
           'rate']


class HttpJsonCheck(AgentCheck):
    def check(self, instance):
        self.log.debug('check {}: runnning'.format(self.name))
        targets_res = get(TARGETS_URL, timeout=10)
        try:
            targets_res.raise_for_status()
        except Exception as e:
            self.warning('check {}: cannot get targets {} {!r}'.format(
                         self.name, TARGETS_URL, e))
            raise
        targets = targets_res.json()
        shuffle(targets)
        for target in targets:
            self.log.debug('check {}: target {}'.format(
                           self.name, target['url']))

            # Multiple instances of dd-agentcheck will be running in production
            # Micros. Only hit endpoint at most once each collection cycle.
            target['min_collection_interval'] = self.min_collection_interval(instance)
            put_res = put(TARGETS_URL, json=target, timeout=10)
            if put_res.status_code == codes.CONFLICT:  # 409
                self.log.debug('check {}: too soon, skipping {}'.format(
                               self.name, target['url']))
                continue
            try:
                put_res.raise_for_status()
            except Exception as e:
                self.warning(
                    'check {}: unexpected failure while getting lock for target {} {!r}'.format(
                    self.name, target['url'], e))
                raise

            hostname = urlparse(target['url']).hostname
            aggregation_key = md5(target['url']).hexdigest()
            timeout = 10

            try:

                try:
                    metrics_by_method = get(target['url'], timeout=timeout)
                except Timeout:
                    self.timeout_event(target['url'], timeout, aggregation_key)
                    raise

                try:
                    metrics_by_method.raise_for_status()
                except:
                    self.status_code_event(target['url'], metrics_by_method, aggregation_key)
                    raise

                for method_name, metrics in metrics_by_method.json().items():
                    if method_name not in METHODS:
                        continue
                    method = getattr(self, method_name)
                    for metric, kwargs in metrics.items():
                        if 'hostname' not in kwargs:
                            kwargs['hostname'] = hostname
                        self.log.debug(
                            'check {}: collected metric for target {}: {} {} {}'.format(
                            self.name, target['url'], method_name, metric, kwargs))
                        method(metric, **kwargs)

            except Exception as e:
                self.warning('check {}: bad target {} {!r}'.format(self.name, target['url'], e))
                continue

    def min_collection_interval(self, instance):
        return instance.get(
            'min_collection_interval', 
            self.init_config.get(
                'min_collection_interval',
                self.DEFAULT_MIN_COLLECTION_INTERVAL))

    def timeout_event(self, url, timeout, aggregation_key):
        self.event({
            'timestamp': int(time()),
            'event_type': 'http_json_check',
            'msg_title': 'URL timeout',
            'msg_text': '{} timed out after {} seconds.'.format(url, timeout),
            'aggregation_key': aggregation_key
        })

    def status_code_event(self, url, res, aggregation_key):
        self.event({
            'timestamp': int(time()),
            'event_type': 'http_json_check',
            'msg_title': 'Invalid reponse code for {}'.format(url),
            'msg_text': '{} returned a status of {}'.format(url, res.status_code),
            'aggregation_key': aggregation_key
        })
